package be.kdg.distrib.testclasses;

/**
 * @author Joël Van Eyken
 * @version 1.0 01.10.20 11:11
 */
public interface TestInterface10 {

  void testMethodNested(PersoonTest persoonTest);
}
