package be.kdg.distrib.testclasses;

/**
 * @author Joël Van Eyken
 * @version 1.0 01.10.20 11:01
 */
public class PersoonTest {

    private String name;
    private int age;
    private AddressTest addressTest;

    public PersoonTest(String name, int age, AddressTest addressTest) {
        this.name = name;
        this.age = age;
        this.addressTest = addressTest;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public AddressTest getAddressTest() {
        return addressTest;
    }

    public void setAddressTest(AddressTest addressTest) {
        this.addressTest = addressTest;
    }
}
