package be.kdg.distrib.testclasses;

/**
 * @author Joël Van Eyken
 * @version 1.0 01.10.20 11:02
 */
public class AddressTest {

    private String street;
    private int nr;

    public AddressTest(String street, int nr) {
        this.street = street;
        this.nr = nr;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }
}
