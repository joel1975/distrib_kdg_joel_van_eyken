package be.kdg.distrib.skeletonFactory;

import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;

import java.lang.reflect.Proxy;
import java.net.InterfaceAddress;

/**
 * @author Joël Van Eyken
 * @version 1.0 03.10.20 12:53
 */
public class SkeletonFactory {

    public static Object createSkeleton(Object obj ) {

        SkeletonInvocationHandler handler = new SkeletonInvocationHandler(obj);

        Object result = Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
                new Class[]{Skeleton.class}, handler);

        return result;
    }
}
