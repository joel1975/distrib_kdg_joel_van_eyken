package be.kdg.distrib.skeletonFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;
import be.kdg.distrib.util.InvokeUtils;

import java.lang.reflect.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author Joël Van Eyken
 * @version 1.0 03.10.20 13:01
 */
public class SkeletonInvocationHandler implements InvocationHandler {

    // == fields ==
    private final MessageManager messageManager;
    private final NetworkAddress address;
    private final Object impl;

    public SkeletonInvocationHandler(Object obj) {
        this.messageManager =  new MessageManager();
        this.address = this.messageManager.getMyAddress();
        this.impl = obj;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        System.out.println("invoke() aangeroepen");
        String methodName = method.getName();
        Object result = null;

        switch(methodName) {

            case "getAddress":
                result = this.address;
                break;

            case "handleRequest":
                // msg received form Stub
                MethodCallMessage receivedMsgHandle = (MethodCallMessage) args[0];
                Method mthHandle = this.getMethodFromImpl(receivedMsgHandle);

                // invoke parameters received from msg on method
                invokeParamsOnMth(receivedMsgHandle, mthHandle);

                // msg reply to Stub
                MethodCallMessage replyRequestMsg = this.sendReply(mthHandle,receivedMsgHandle);
                this.messageManager.send(replyRequestMsg, receivedMsgHandle.getOriginator());
                break;

            case "run":
                Thread thread = new Thread(() -> {
                    while(true) {
                        MethodCallMessage receivedMsgRun = this.messageManager.wReceive();
                        Method mthRun = this.getMethodFromImpl(receivedMsgRun);
                        invokeParamsOnMth(receivedMsgRun, mthRun);

                        MethodCallMessage replyRunMsg = this.sendReply(mthRun,receivedMsgRun);
                        this.messageManager.send(replyRunMsg, receivedMsgRun.getOriginator() );
                    }
                });
                thread.start();
                break;
        }
        return result;
    }

    // == invoke a Object array on the mth  ==
    private void invokeParamsOnMth(MethodCallMessage msg, Method mth) {

        Object[] args = this.createObjectsFromParams(mth, msg);
        try {
            mth.invoke(impl, args);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    // == find all parameters form msg convert to Objects (array) ==
    private Object[] createObjectsFromParams(Method mth, MethodCallMessage msg){

        Map<String, List<Map.Entry<String, String>>> argMap = this.checkParamsAndCreateMap(msg);
        Parameter[] mthParams = mth.getParameters();
        Object[] objs = new Object[argMap.size()];

        IntStream.range(0, argMap.size()).forEach( index -> {
            List<Map.Entry<String, String>> entries = argMap.get("arg"+ index);

            Parameter param = mthParams[index];     // parameter =  example : int arg0
//            Parameter param = Arrays.stream(mthParams)
//                    .filter( p -> p.getName().contains("arg"+ index))
//                    .peek(System.out::println)
//                    .findFirst()
//                    .orElseThrow(() -> new IllegalArgumentException("Wrong parameter key"));
//                    // not possible => checked in checkParamsAndCreateMap

            Class<?> type = param.getType();
            Object obj = null;

            // == parameter type is primitive or String
            if (InvokeUtils.primitiveTypes.contains(type)) {
                objs[index] = InvokeUtils.transformTypeToObjectWithGivenValue(type, entries.get(0).getValue());

            // == parameter is POJO Object
            } else {
                try {
                    objs[index] = this.createObjectFromEntries(entries, type);
                } catch (NoSuchMethodException | IllegalAccessException |
                        InvocationTargetException | InstantiationException e) {
                    e.printStackTrace();
                }
            }
        });

        return objs;
    }

    // == convert msg parameters to Map with key "arg? " and value a "List with entrySet"  ==
    private Map<String, List<Map.Entry<String, String>>> checkParamsAndCreateMap(MethodCallMessage msg) {

        boolean isParamNameCorrect = msg.getParameters().keySet()
                .stream().allMatch(key -> key.startsWith("arg"));

        if (isParamNameCorrect) {
            return msg.getParameters().entrySet()
                    .stream()
                    .collect(Collectors.groupingBy(entry -> entry.getKey().split("\\.")[0]));
        } else {
            throw new RuntimeException("wrong parameter key");
        }
    }

    private Object createObjectFromEntries(List<Map.Entry<String, String>> entries, Class<?> type)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        Object obj = type.getDeclaredConstructor().newInstance();
        Method[] methods = obj.getClass().getMethods();

         entries.forEach(entry -> {

             String keyName = entry.getKey().split("\\.")[1];
             String setMth = "set" + keyName.substring(0, 1).toUpperCase() + keyName.substring(1);

             Optional<Method> optionalMethod = Arrays.stream(methods)
                 .filter(m -> m.getName().contains(setMth))
                 .findFirst();

             // == Object has set method ==
            if (optionalMethod.isPresent()) {

                Method method = optionalMethod.get();
                Class<?>[] parameterTypes = method.getParameterTypes();

                Arrays.stream(parameterTypes).forEach( paramType -> {
                    try {
                        method.invoke(obj, InvokeUtils.transformTypeToObjectWithGivenValue( paramType, entry.getValue()));
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                });
            } else {
                // == Object only has get method (Field access used) ==
                try {
                    Field field = type.getDeclaredField(keyName);
                    field.setAccessible(true);
                    field.set(obj, InvokeUtils.transformTypeToObjectWithGivenValue(field.getGenericType(),entry.getValue()));
                } catch (NoSuchFieldException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
         });

        return obj;
    }

    // check if method return void , primitive or Object and put it in msg
    private MethodCallMessage sendReply(Method method, MethodCallMessage msg) {

        MethodCallMessage reply = new MethodCallMessage(this.messageManager.getMyAddress(), "result");
        Class<?> returnType = method.getReturnType();

        // for method with no return value
        if (returnType == void.class) {
            reply.setParameter("result", "Ok");
        } else {

            // for method with parameters and return primitive or Object
            Object[] objs = this.createObjectsFromParams(method,msg);
            try {
                Object obj = method.invoke(this.impl,objs);

                // return primitive
                if (InvokeUtils.primitiveTypes.contains(obj.getClass())) {
                    reply.setParameter("result", obj.toString());
                // return Object
                } else {
                    reply = this.fromObjectToMsg(obj, reply);
                }

            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        return reply;
    }

    // == takes a Object and msg and put the object attributes in msg
    private MethodCallMessage fromObjectToMsg(Object obj, MethodCallMessage msg) {

        Arrays.stream(obj.getClass().getDeclaredMethods())
                .filter(m -> m.getName().startsWith("get") || m.getName().startsWith("is"))
                .map(m -> {
                    try {
                        Object invoke = m.invoke(obj);
                        return new Object[]{invoke, m.getName() };
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                       throw new RuntimeException(e.getMessage());
                    }
                })
                .forEach( array -> {
                    String result = array[1].toString();
                    result = result.startsWith("is") ? result.substring(2) : result.substring(3);
                    result  = "result." + result.substring(0,1).toLowerCase() + result.substring(1);
                    msg.setParameter(result, array[0].toString());
                });
        return msg;
    }

    private Method getMethodFromImpl(MethodCallMessage msg) {

        String methodName = msg.getMethodName();

        Method[] methods = this.impl.getClass().getMethods();
        return Arrays.stream(methods)
                .filter(m -> m.getName().equals(methodName))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Method " + methodName + "not found!"));
    }
}
