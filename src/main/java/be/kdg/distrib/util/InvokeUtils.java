package be.kdg.distrib.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

/**
 * @author Joël Van Eyken
 * @version 1.0 04.10.20 18:37
 */
public class InvokeUtils {

    public static final List<Type> primitiveTypes = Arrays.asList(new Type[] {Integer.class, Boolean.class, Character.class,
            Double.class, Long.class, Float.class, Byte.class, String.class, int.class, boolean.class, char.class,
            double.class, long.class, float.class, byte.class});

    public static Object transformTypeToObjectWithGivenValue(Type returnType, String parameter) {

        String returnTypeName = returnType.getTypeName();
        return switch(returnTypeName) {
            case "int"     -> Integer.parseInt(parameter);
            case "double"  -> Double.parseDouble(parameter);
            case "long"    -> Long.parseLong(parameter);
            case "short"   -> Short.parseShort(parameter);
            case "byte"    -> Byte.parseByte(parameter);
            case "boolean" -> Boolean.parseBoolean(parameter);
            case "float"   -> Float.parseFloat(parameter);
            case "char"    -> parameter.charAt(0);
            default        -> parameter;  // == String
        };
    }

//    public static void invokeMthOnObject(Object obj, Method mth, String value)
//            throws InvocationTargetException, IllegalAccessException {
//
//        Class<?> parameterType = mth.getParameterTypes()[0];
//
//        switch (parameterType.toString()) {
//            case "int" :
//                int i = Integer.parseInt(value);
//                mth.invoke(obj, i);
//                break;
//            case "char":
//                char c = value.charAt(0);
//                mth.invoke(obj, c);
//                break;
//            case "double":
//                double d = Double.parseDouble(value);
//                mth.invoke(obj,d);
//                break;
//            case "short":
//                Short s = Short.parseShort(value);
//                mth.invoke(obj,s);
//                break;
//            case "long":
//                Long l = Long.parseLong(value);
//                mth.invoke(obj,l);
//                break;
//            case "float":
//                Float f = Float.parseFloat(value);
//                mth.invoke(obj,f);
//                break;
//            case "byte":
//                Byte b = Byte.parseByte(value);
//                mth.invoke(obj, b);
//                break;
//            case "boolean":
//                boolean bo = Boolean.parseBoolean(value);
//                mth.invoke(obj,bo);
//                break;
//            default:
//                mth.invoke(obj, value);
//        }
//    }
}
