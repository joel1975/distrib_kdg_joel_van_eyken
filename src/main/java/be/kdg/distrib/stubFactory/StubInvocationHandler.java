package be.kdg.distrib.stubFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.MethodCallMessage;
import be.kdg.distrib.communication.NetworkAddress;
import be.kdg.distrib.util.InvokeUtils;

import java.lang.reflect.*;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author Joël Van Eyken
 * @version 1.0 30.09.20 11:52
 */
public class StubInvocationHandler implements InvocationHandler {

    // == fields ==
    private final MessageManager messageManager;
    private final NetworkAddress serverAddress;

    public StubInvocationHandler(MessageManager messageManager, NetworkAddress serverAddress) {

       this.messageManager = messageManager;
       this.serverAddress = serverAddress;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        System.out.println("invoke() aangeroepen");
        Class<?>[] parameterTypes = method.getParameterTypes();
        Class<?> returnType = method.getReturnType();

        MethodCallMessage msg = new MethodCallMessage(messageManager.getMyAddress(), method.getName());

        Object result = null;

        if (parameterTypes.length == 0) {
            messageManager.send(msg, this.serverAddress);

        } else {
            MethodCallMessage  msgWithArgs = this.sendMsgWithArgs(args, msg);
            messageManager.send(msgWithArgs, this.serverAddress);

        }

        if (returnType == void.class) {
            MethodCallMessage mthCallMsg = this.messageManager.wReceive();
            System.out.println("Result empty reply: " + mthCallMsg.getParameter("result"));
        }

        if ( returnType != void.class) {

            MethodCallMessage mthCallMsg = this.messageManager.wReceive();
            System.out.println("Result reply: " + mthCallMsg);

            if (InvokeUtils.primitiveTypes.contains(returnType)) {

                result = InvokeUtils.transformTypeToObjectWithGivenValue(returnType, mthCallMsg.getParameter("result"));

                // return value is POJO ==
            } else {

                Object obj = returnType.getDeclaredConstructor().newInstance();
                List<Method> methods = Arrays.stream(returnType.getDeclaredMethods())
                        .collect(Collectors.toList());

                mthCallMsg.getParameters().forEach((k,v) -> {
                    String name = k.split("\\.")[1];
                    String mthSetName = "set" + name.substring(0,1).toUpperCase() + name.substring(1);
                    Optional<Method> mth = methods.stream()
                            .filter( m -> m.getName().startsWith(mthSetName))
                            .findAny();

                    // == Object has set method ==
                    if (mth.isPresent()) {
                        try {

                            Class<?> parameterType = mth.get().getParameterTypes()[0];
                            Object value = InvokeUtils.transformTypeToObjectWithGivenValue(parameterType, v);
                            mth.get().invoke(obj, value);
                        } catch (InvocationTargetException | IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    // == Object only has get method (Field access used) ==
                    } else {
                        try {
                            Field field = returnType.getDeclaredField(name);
                            field.setAccessible(true);
                            field.set(obj, InvokeUtils.transformTypeToObjectWithGivenValue(field.getGenericType(), v));
                        } catch (NoSuchFieldException | IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                });
                result = obj;
            }
        }
        return result;
    }

    // == private Methods ==
    private MethodCallMessage sendMsgWithArgs(Object[] args,  MethodCallMessage msg ){

       IntStream.range(0, args.length).forEach( index -> {

           Class argClass = args[index].getClass();

           // == for primitive type and String ==
           if (InvokeUtils.primitiveTypes.contains(argClass)) {
               msg.setParameter("arg"+index, args[index].toString());
           // == for POJO ==
           } else {
              this.argToObject(args[index], msg, "arg" + index);
           }
        });
        return msg;
    }

    private void argToObject(Object obj, MethodCallMessage msg, String preText) {

        Class objClass = obj.getClass();
        List<Method> methods = Arrays.stream(objClass.getDeclaredMethods())
                .filter(m -> m.getName().startsWith("get") || m.getName().startsWith("is"))
                .collect(Collectors.toList());

        IntStream.range(0, methods.size()).forEach( i -> {
            try {
                Object value = methods.get(i).invoke(obj);
                String name = methods.get(i).getName().startsWith("is") ? methods.get(i).getName().substring(2) :
                        methods.get(i).getName().substring(3);  // is or get ?
                name = name.toLowerCase();

                if (!InvokeUtils.primitiveTypes.contains(value.getClass())) {
                    // another POJO
                    this.argToObject(value, msg, preText + "." +name);
                } else {
                    // primitive types + String
                    msg.setParameter(preText + "." + name, value.toString());
                }

            } catch ( IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        });
    }
}
