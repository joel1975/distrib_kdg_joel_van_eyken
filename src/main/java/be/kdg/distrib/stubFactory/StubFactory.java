package be.kdg.distrib.stubFactory;

import be.kdg.distrib.communication.MessageManager;
import be.kdg.distrib.communication.NetworkAddress;

import java.lang.reflect.Proxy;

/**
 * @author Joël Van Eyken
 * @version 1.0 30.09.20 11:45
 */
public class StubFactory {

    public static Object createStub(Class myInterface, String ipAddress, int port) {

        NetworkAddress networkAddress = new NetworkAddress(ipAddress, port);
        MessageManager msgManager = new MessageManager();
        StubInvocationHandler handler = new StubInvocationHandler(msgManager, networkAddress);

        Object result = Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(),
                new Class[]{myInterface}, handler);

        return result;
    }
}
